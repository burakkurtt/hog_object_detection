"""
    This is uav detection implementation using Hog transformation and SVM algorithm. Code taken from
    LearnOpenCV.com and edited for uav description. This algorithm takes images,
    extract the hog feature of these images and train hog features using SVM classification
    method and create model.
"""

import cv2
import numpy as np
from os.path import isfile, join, exists
from os import listdir, makedirs, remove
from shutil import copyfile
from numpy.random import shuffle

SIZE = 50

# Dataset generator class. This class generate label for data
class DataSetGenerator:
    def __init__(self, data_dir):
        self.data_dir = data_dir
        self.data_labels = self.get_data_labels()
        self.data_info = self.get_data_paths()

    def get_data_labels(self):
        data_labels = []
        for filename in listdir(self.data_dir):
            if not isfile(join(self.data_dir, filename)):
                data_labels.append(filename)
        return data_labels

    def get_data_paths(self):
        data_paths = []
        for label in self.data_labels:
            img_list = []
            path = join(self.data_dir, label)
            for filename in listdir(path):
                tokens = filename.split('.')
                if tokens[-1] == 'png':
                    image_path = join(path, filename)
                    img_list.append(image_path)
            shuffle(img_list)
            data_paths.append(img_list)
        return data_paths

    def get_labeled_dataset(self, image_size=(SIZE, SIZE), allchannel=False):
        images = []
        labels = []
        counter = 0
        empty = False
        while True:
            for i in range(len(self.data_labels)):
                #label = np.zeros(len(self.data_labels), dtype=int)
                #label[i] = 1
                if len(self.data_info[i]) < counter+1:
                    empty = True
                    continue
                empty = False
                img = cv2.imread(self.data_info[i][counter])
                img = self.resizeAndPad(img, image_size)
                if not allchannel:
                    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    img = np.reshape(img, (img.shape[0], img.shape[1], 1))
                images.append(img)
                labels.append(i)
                #labels.append(label)
            counter += 1

            if empty:
                break
        return images, labels

    def resizeAndPad(self, img, size):
        h, w = img.shape[:2]

        sh, sw = size
        # interpolation method
        if h > sh or w > sw:  # shrinking image
            interp = cv2.INTER_AREA
        else: # stretching image
            interp = cv2.INTER_CUBIC

        # aspect ratio of image
        aspect = w/h

        # padding
        if aspect > 1: # horizontal image
            new_shape = list(img.shape)
            new_shape[0] = w
            new_shape[1] = w
            new_shape = tuple(new_shape)
            new_img=np.zeros(new_shape, dtype=np.uint8)
            h_offset=int((w-h)/2)
            new_img[h_offset:h_offset+h, :, :] = img.copy()

        elif aspect < 1: # vertical image
            new_shape = list(img.shape)
            new_shape[0] = h
            new_shape[1] = h
            new_shape = tuple(new_shape)
            new_img = np.zeros(new_shape,dtype=np.uint8)
            w_offset = int((h-w) / 2)
            new_img[:, w_offset:w_offset + w, :] = img.copy()
        else:
            new_img = img.copy()
        # scale and pad
        scaled_img = cv2.resize(new_img, size, interpolation=interp)
        return scaled_img

class StatModel():
    def load(self, fn):
        self.model.load(fn)
    def save(self, fn):
        self.model.save(fn)

class SVM(StatModel):
    def __init__(self, C = 12.5, gamma = 0.50625):
        self.model = cv2.ml.SVM_create()
        self.model.setGamma(gamma)
        self.model.setC(C)
        self.model.setKernel(cv2.ml.SVM_RBF)
        self.model.setType(cv2.ml.SVM_C_SVC)

    def train(self, samples, response):
        self.model.train(samples, cv2.ml.ROW_SAMPLE, response)

    def predict(self, samples):
        return self.model.predict(samples)[1].ravel()

# This function takes raw images direction and give outputs as images and its
# labels
def load_uavs(data_dir):
    """ Uav (quadcopter and fix wing picture and labels will be added) """

    # Seperate mixed image to individual class files wth class nemas
    seperateData(data_dir)
    # Dataset generation
    dataset = DataSetGenerator(data_dir)
    # print Class
    labels = dataset.get_data_labels()
    print '\nData have below class: \n', [item for item in labels]
    # Print first 5 dataset info
    info = dataset.get_data_paths()
    #print '\nData sample info: \n', info
    # Data labelling
    imgs, labels = dataset.get_labeled_dataset()
    #print(imgs)
    #print(labels)

    return imgs, labels

# Seperating the images in same directory depend on images filenames. Each '.'
# in filename is delinumerator. Filenames is seperated with dots and if last seperated
# is 'png', this file taken as image and sended first seperated name file, if there is not
# same named file in directory. New file is created.
def seperateData(data_dir):
    # image format - [drone/fixedWing].[sample number].png
    for filename in listdir(data_dir):
        if isfile(join(data_dir, filename)):
            tokens = filename.split('.')
            if tokens[-1] == 'png':
                image_path = join(data_dir, filename)
                if not exists(join(data_dir, tokens[0])):
                    makedirs(join(data_dir, tokens[0]))
                copyfile(image_path, join(join(data_dir, tokens[0]), filename))
                remove(image_path)

def deskew(img):
    m = cv2.moments(img)
    if abs(m['mu02']) < 1e-2:
        return img.copy()
    skew = m['mu11']/m['mu02']
    M = np.float32([[1, skew, -0.5*SIZE*skew], [0, 1, 0]])
    img = cv2.warpAffine(img, M, (SIZE, SIZE), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR)
    return img

def evaluate_model(model, uavs, samples, labels):
    resp = model.predict(samples)
    err = (labels != resp).mean()
    print 'Accuracy: ', (1-err)*100 
    confusion = np.zeros((10, 10))

def prediction_new_img(model, predicted_img):
    img = []
    predicted_img = cv2.resize(predicted_img, (SIZE, SIZE))
    predicted_img_gray = cv2.cvtColor(predicted_img, cv2.COLOR_BGR2GRAY)
    img.append(hog.compute(predicted_img_gray))
    img = np.asarray(img)
    prediction = model.predict(img)
    if prediction == 1:
        label = 'drone'
    elif prediction == 0:
        label = 'fixedWing'
            
    print "Prediction class of new image : ", label
    cv2.imshow("Predict", predicted_img)
    cv2.waitKey(0)



def preprocess_simple(uavs):
    return np.float32(uavs).reshape(-1, SIZE*SIZE) / 255.0

def get_hog():
    winSize = (20,20)
    blockSize = (10,10)
    blockStride = (5,5)
    cellSize = (10,10)
    nbins = 9
    derivAperture = 1
    winSigma = -1.
    histogramNormType = 0
    L2HysThreshold = 0.2
    gammaCorrection = 1
    nlevels = 64
    signedGradient = True

    hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,  \
                            derivAperture,winSigma,histogramNormType,      \
                            L2HysThreshold,gammaCorrection,nlevels,        \
                            signedGradient)

    return hog

if __name__ == '__main__':

    # Define raw images directions
    DATA_DIR = './capture_img/mix_imgs/'

    print('Loading data from dataset ... ')
    # Data will be loaded
    uavs, labels = load_uavs(DATA_DIR)

    print('Shuffle data ... ')
    # Shuffle data
    indx = np.arange(len(labels))
    np.random.shuffle(indx)
    uavs = [uavs[i] for i in indx]
    labels = [labels[i] for i in indx]

    print('Deskew images ... ')
    uavs_dewkewed = list(map(deskew, uavs))

    print('Defining HoG parameters ... ')
    # HoG feature hog_descriptors
    hog = get_hog()

    print('Calculating HoG descriptor for every image ... ')
    hog_descriptors = []
    for img in uavs_dewkewed:
        hog_descriptors.append(hog.compute(img))
    hog_descriptors = np.squeeze(hog_descriptors)

    print('Splitting data into training (90%) and test set (10%) ... ')
    train_n = int(0.8*len(hog_descriptors))
    uavs_train, uavs_test = np.split(uavs_dewkewed, [train_n])
    hog_descriptors_train, hog_descriptors_test = np.split(hog_descriptors, [train_n])
    labels_train, labels_test = np.split(labels, [train_n])

    print('Training SVM model ... ')
    model = SVM()
    labels_train = np.asarray(labels_train)
    model.train(hog_descriptors_train, labels_train)

    print('Evaluating model ... ')
    evaluate_model(model, uavs_test, hog_descriptors_test, labels_test)

    print('Saving SVM model ... ')
    model.save('uav_SVM.xml')

    print('Predict image ...')
    # predictee image directory should be given
    img =  cv2.imread('./capture_img/test_img/test2.jpg')
    
    if img is not None:
        prediction_new_img(model, img)
    elif img is None:
        print ("Error loading image")
    

    
